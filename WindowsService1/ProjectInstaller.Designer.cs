﻿namespace WindowsService1
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.jeeto365DaySettlementServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.jeeto365DaySettlementServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // jeeto365DaySettlementServiceProcessInstaller
            // 
            this.jeeto365DaySettlementServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.jeeto365DaySettlementServiceProcessInstaller.Password = null;
            this.jeeto365DaySettlementServiceProcessInstaller.Username = null;
            this.jeeto365DaySettlementServiceProcessInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.jeeto365DayServiceInstaller_AfterInstall);
            // 
            // jeeto365DaySettlementServiceInstaller
            // 
            this.jeeto365DaySettlementServiceInstaller.Description = "Settlement Service";
            this.jeeto365DaySettlementServiceInstaller.DisplayName = "Settlement Service";
            this.jeeto365DaySettlementServiceInstaller.ServiceName = "Jeeto365DaySettlementService";
            this.jeeto365DaySettlementServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.jeeto365DaySettlementServiceInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.jeeto365DayServiceInstaller_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.jeeto365DaySettlementServiceProcessInstaller,
            this.jeeto365DaySettlementServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller jeeto365DaySettlementServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller jeeto365DaySettlementServiceInstaller;
    }
}